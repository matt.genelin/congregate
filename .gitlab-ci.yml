stages:
  - build
  - test
  - rolling release
  - deploy
  - bump version
  - release

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - local: ci/vault.yml

variables:
  DOCKER_VERSION: "19.03.1"
  TEST_IMAGE_NAME: "$CI_REGISTRY_IMAGE/test-3.8:latest"
  BB_SERVER_SEED_IMAGE_NAME: "$CI_REGISTRY_IMAGE/bitbucket-seed:latest"
  JENKINS_SEED_IMAGE_NAME: "$CI_REGISTRY_IMAGE/jenkins-seed:latest"
  SAST_EXCLUDED_ANALYZERS: "brakeman,flawfinder,gosec,kubesec,mobsf,phpcs-security-audit,pmd-apex,security-code-scan,sobelow,spotbugs"
  SECURE_LOG_LEVEL: debug
  CONGREGATE_PATH: $CI_PROJECT_DIR
  APP_PATH: $CI_PROJECT_DIR

default:
  image: "$TEST_IMAGE_NAME"
  tags:
    - congregate-auto-scale-runner

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: "$CI_PIPELINE_SOURCE == 'merge_request_event'"
    # For `master` branch, create a pipeline (this excludes schedules but includes pushes, merges, etc.).
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
    # For tags, create a pipeline.
    - if: "$CI_COMMIT_TAG"
    # For pipeline schedules, create a pipeline
    - if: "$CI_PIPELINE_SOURCE == 'schedule'"

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
  # tags:
  #   # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
  #   - gitlab-org-docker

.docker-login:
  before_script:
    - docker login ${CI_REGISTRY} -u gitlab-ci-token -p $CI_BUILD_TOKEN

.gitlab-destination:
  extends: .gitlab-license
  variables:
    GITLAB_LICENSE_PATH: "$GITLAB_LICENSE"
    GITLAB_OMNIBUS_CONFIG: |-
      gitlab_rails['initial_root_password'] = '5iveL!fe'
      gitlab_rails['initial_license_file'] = '/etc/gitlab/Gitlab.gitlab-license'
  services:
    - name: gitlab/gitlab-ee:nightly
      alias: gitlab_destination
      command: [
          "sh",
          "-c",
          "mkdir -p /etc/gitlab & \
          echo $GITLAB_LICENSE_PATH > /etc/gitlab/Gitlab.gitlab-license & \
          python3 -m http.server 443 --directory /var/log/gitlab/gitlab-rails/ & :; /assets/wrapper",
        ]
    - name: mongo:4.4.4
      alias: mongo

.e2e-ghe-cleanup:
  after_script:
    - pwsh --version
    - pwsh -f ./azure_scripts/delete_azure_vm.ps1

.e2e-cleanup:
  after_script:
    - curl -O http://gitlab_destination:443/api_json.log
    - curl -O http://gitlab_destination:443/importer.log
    - curl -O http://gitlab_destination:443/exceptions_json.log
    - poetry run aws ec2 terminate-instances --instance-ids $(cat instance_id)
  artifacts:
    when: always
    paths:
      - data/**/*.html
      - data/**/*diff*.json
      - data/**/*results.json
      - data/**/audit*.log
      - api_json.log
      - importer.log
      - exceptions_json.log

.jenkins-test:
  services:
    - name: "$JENKINS_SEED_IMAGE_NAME"
      alias: jenkins-test
      variables:
        SEED_DATA: "true"

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]
  tags:
    - gitlab-org-docker
  rules:
    - if: "$CODE_QUALITY_DISABLED"
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run code quality job in merge request pipelines
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'" # Run code quality job in pipelines on the master branch (but not in other branch pipelines)
    - if: "$CI_COMMIT_TAG" # Run code quality job in pipelines for tags
    - changes:
        - ./**/*.py
        - ./*.py
        - ./**/*.js

secret_detection:
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"

version check:
  stage: build
  script:
    - dev/bin/version_check.sh
  rules:
    - if: "$CI_COMMIT_TAG"

build:
  extends:
    - .use-docker-in-docker
    - .docker-login
  stage: build
  script:
    - cp ./docker/test/py38.Dockerfile ./Dockerfile
    - docker build --tag "$TEST_IMAGE_NAME" .
    - docker push "$TEST_IMAGE_NAME"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - docker/test/*

build bitbucket seed image:
  extends:
    - .use-docker-in-docker
    - .docker-login
    - .bbs-creds
  stage: build
  script:
    - cp docker/bitbucket/Dockerfile ./
    - DOCKER_BUILDKIT=1 docker build -t $BB_SERVER_SEED_IMAGE_NAME . --build-arg BITBUCKET_LICENSE=$BITBUCKET_LICENSE --build-arg BITBUCKET_PASSWORD=$BITBUCKET_PASSWORD
    - docker push "$BB_SERVER_SEED_IMAGE_NAME"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - docker/bitbucket/Dockerfile
        - docker/bitbucket/entrypoint.py
        - docker/bitbucket/seed_data.sh

build jenkins seed image:
  extends:
    - .use-docker-in-docker
    - .docker-login
  stage: build
  script:
    - cd docker/jenkins
    - DOCKER_BUILDKIT=1 docker build -t $JENKINS_SEED_IMAGE_NAME .
    - docker push "$JENKINS_SEED_IMAGE_NAME"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - docker/jenkins/Dockerfile
        - docker/jenkins/entrypoint.sh
        - docker/jenkins/jenkins.py
        - docker/jenkins/setup_and_seed.py

check ldap compose yaml:
  extends:
    - .use-docker-in-docker
    - .docker-login
  stage: build
  script:
    - cd docker/ldap
    - docker info
    - apk update
    - apk upgrade
    - apk add curl
    - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile default -y
    - source $HOME/.cargo/env
    - apk add python3 py-pip python3-dev libffi-dev openssl-dev gcc libc-dev rust cargo make
    - /usr/bin/python3.7 -m pip install --upgrade pip
    - pip3 install docker-compose
    - docker-compose -f docker-compose.yml config
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - docker/ldap/**/*

.test-job:
  stage: test
  before_script:
    - pip install poetry
    # Disable parallel installer to avoid "Key Error" issues
    - poetry config experimental.new-installer false
    - poetry install
    - ./congregate.sh init
  retry: 2
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - congregate/**/*
        - docker/test/*
        - congregate.sh
        - pyproject.toml
        - poetry.lock
        - ci/spin_up_test_vm.sh
        - package.json
        - ci/pylint*.sh
        - .pylintrc
        - delete_azure_vm.ps1
        - restore_azure_vm.sh
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      when: always

.e2e-test-setup:
  image: "${TEST_IMAGE_NAME}"
  extends:
    - .use-docker-in-docker
    - .gitlab-destination
    - .e2e-cleanup
    - .aws-creds
    - .gitlab-api-creds
  tags:
    - congregate-auto-scale-runner
  before_script:
    - !reference [.test-job, before_script]
    - ci/spin_up_test_vm.sh
    - export GITLAB_SRC=http://$(cat source_ip)
    - export GITLAB_SRC_REG_URL=$(cat source_ip):4567
    - export GITLAB_DEST=http://gitlab_destination
    - export GITLAB_DEST_REG_URL=gitlab_destination:4567
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - .gitlab-ci.yml
        - congregate/helpers/**/*
        - congregate/migration/gitlab/**/*
        - congregate/migration/migrate.py
    - if: "$CI_PIPELINE_SOURCE == 'schedule' && $E2E_NIGHTLY"
  timeout: 30 minutes

.fallback-e2e-cleanup:
  image: "${TEST_IMAGE_NAME}"
  when: on_failure
  before_script:
    - !reference [.test-job, before_script]
  script:
    - poetry run aws ec2 terminate-instances --instance-ids $(cat $INSTANCE_ID_FILE)
  tags:
    - congregate-auto-scale-runner

test:pylint:
  extends:
    - .test-job
    - .gitlab-api-creds
  script:
    - ci/pylint.sh
  artifacts:
    paths:
      - pylint.txt

test:pylint:syntax-errors:
  extends: .test-job
  script:
    - ci/pylint-errors.sh
  artifacts:
    paths:
      - pylint.txt
    when: on_failure

test:unit:
  extends: .test-job
  script:
    - poetry run pytest -m 'unit_test' --cov-report html --cov-report term-missing --cov-config=.coveragerc --cov=congregate congregate/tests/
  timeout: 5 minutes
  artifacts:
    paths:
      - htmlcov/*

test:end-to-end full:
  extends:
    - .e2e-test-setup
  script:
    - cp instance_id full_instance_id
    - poetry run pytest -s -m e2e_gl_setup congregate/tests/
    - poetry run pytest -s -m e2e --cov-config=.coveragerc --cov=congregate congregate/tests/
  timeout: 30 minutes
  cache:
    key: "$CI_COMMIT_SHA-full"
    paths:
      - full_instance_id
    when: always

test:end-to-end single group:
  extends:
    - .e2e-test-setup
  script:
    - cp instance_id sg_instance_id
    - poetry run pytest -s -m e2e_gl_setup_2 congregate/tests/
    - poetry run pytest -s -m e2e --cov-config=.coveragerc --cov=congregate congregate/tests/
  timeout: 30 minutes
  cache:
    key: "$CI_COMMIT_SHA-sg"
    paths:
      - sg_instance_id
    when: always

test:failed-e2e-full-cleanup:
  variables:
    INSTANCE_ID_FILE: full_instance_id
  needs:
    - job: test:end-to-end full
      optional: true
  extends:
    - .fallback-e2e-cleanup
    - .aws-creds
  when: on_failure
  rules:
    - !reference [.e2e-test-setup, rules]
  cache:
    key: "$CI_COMMIT_SHA-full"
    paths:
      - full_instance_id
    policy: pull

test:failed-e2e-single-group-cleanup:
  variables:
    INSTANCE_ID_FILE: sg_instance_id
  needs:
    - job: test:end-to-end single group
      optional: true
  extends:
    - .fallback-e2e-cleanup
    - .aws-creds
  rules:
    - !reference [.e2e-test-setup, rules]
  cache:
    key: "$CI_COMMIT_SHA-sg"
    paths:
      - sg_instance_id
    policy: pull

test:Jenkins Integration Tests:
  extends:
    - .jenkins-test
    - .test-job
  image: "${TEST_IMAGE_NAME}"
  tags:
    - congregate-auto-scale-runner
  script:
    - while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://jenkins-test:8080/job/test-job/config.xml --user $JENKINS_USER:$JENKINS_PASSWORD)" != "200" ]]; do echo "Waiting for Jenkins to start"; sleep 5; done
    - curl http://jenkins-test:8080/job/test-job/config.xml --user $JENKINS_USER:$JENKINS_PASSWORD
    - echo "Jenkins is up and running, starting pytest"
    - poetry run pytest -s -m "jenkins_it" congregate/tests/
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - congregate/migration/jenkins/**/*
  timeout: 10 minutes

.docker-build:
  extends:
    - .use-docker-in-docker
    - .docker-login
  script:
    - |
      cp docker/release/$DISTRO.Dockerfile ./Dockerfile
      DOCKER_BUILDKIT=1 docker build --tag "$RELEASE_IMAGE_NAME" .
      docker push "$RELEASE_IMAGE_NAME"
      if [ -n "$LATEST" ]
      then 
        docker image tag $RELEASE_IMAGE_NAME $LATEST
        docker push "$LATEST"
      fi

bump version:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: bump version
  extends:
    - .release-creds
  script:
    - apk --update add python3 py-pip git
    - pip install poetryupvers
    - ppuv bump
    - ppuv generate-release-notes --save --path=./notes.md
    - ppuv push-bump --config-user=$BOT_USER --config-email=$BOT_EMAIL --remote=origin --url=$PUSH_URL --branch="HEAD:$CI_COMMIT_BRANCH"
    - release-cli create --name "$(cat VERSION)" --ref $(git rev-parse HEAD) --tag-name "$(cat VERSION)" --description "$(cat notes.md)"
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /Bump version/
      when: never
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: manual

debian rolling release:
  extends:
    - .docker-build
  stage: rolling release
  variables:
    DISTRO: "debian"
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:rolling-debian"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - congregate/**/*.py
        - frontend/**/*
        - congregate.sh
        - pyproject.toml
        - package.json
        - docker/release/debian.Dockerfile

debian-release-tag:
  extends:
    - .docker-build
  rules:
    - if: "$CI_COMMIT_TAG"
  stage: release
  variables:
    DISTRO: "debian"
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-debian"
    LATEST: "$CI_REGISTRY_IMAGE:latest-debian"

centos rolling release:
  extends:
    - .docker-build
  stage: rolling release
  variables:
    DISTRO: "centos"
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:rolling-centos"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - congregate/**/*.py
        - frontend/**/*
        - congregate.sh
        - pyproject.toml
        - package.json
        - docker/release/centos.Dockerfile

centos-release-tag:
  extends:
    - .docker-build
  rules:
    - if: "$CI_COMMIT_TAG"
  stage: release
  variables:
    DISTRO: "centos"
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-centos"
    LATEST: "$CI_REGISTRY_IMAGE:latest-centos"

container-scan-build:centos:
  stage: build
  extends:
    - .docker-build
  variables:
    DISTRO: "centos"
    RELEASE_IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA-centos
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - pyproject.toml
        - package.json
        - docker/release/centos.Dockerfile

container-scan-build:debian:
  stage: build
  extends:
    - .docker-build
  variables:
    DISTRO: "debian"
    RELEASE_IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA-debian
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - pyproject.toml
        - package.json
        - docker/release/debian.Dockerfile

# Scanning the centos image
container_scanning:
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA-centos
    DOCKERFILE_PATH: "docker/release/centos.Dockerfile"
    GIT_STRATEGY: "fetch"
  script:
    - gtcs scan
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - pyproject.toml
        - package.json
        - docker/release/centos.Dockerfile
        - docker/release/debian.Dockerfile

debian_container_scanning:
  extends: container_scanning
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA-debian
    DOCKERFILE_PATH: "docker/release/debian.Dockerfile"
    GIT_STRATEGY: "fetch"
  tags:
    - gitlab-org-docker
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - pyproject.toml
        - package.json
        - docker/release/debian.Dockerfile

docs update:
  stage: deploy
  extends:
    - .dotcom-creds
  script:
    - echo "This is a placeholder job ran only when runbooks are changed"
    - echo "Storing any changed runbook to artifacts that will expire in 1 week."
    # To the ps-migration project
    - python ci/docscript.py 11750578
    # To the Migration Template
    - python ci/docscript.py 21146917 extras
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - runbooks/*
        - customer/*
  artifacts:
    paths:
      - runbooks/*
      - customer/*
    expire_in: 1 week

mvc:
  stage: test
  script:
    - echo "Placeholder job to trigger a min. pipeline for non-code related changes"
  rules:
    - if: "($CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH) && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - ./**/*.md
        - ./*.md
        - docker/release/*
        - .gitlab-ci.yml

pages:
  stage: deploy
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      changes:
        - ./**/*.md
        - ./*.md
        - mkdocs.yml
  dependencies:
    - code_quality
  script:
    - mkdir -p data
    - poetry config experimental.new-installer false
    - poetry install
    # TODO
    # - cp gl-code-quality-report.json congregate/docs/source/_static/
    - poetry run mkdocs build
    - mv site public
  retry: 2
  artifacts:
    paths:
      - public

test:GitHub Enterprise end to end:
  stage: test
  extends:
    - .test-job
    - .e2e-ghe-cleanup
    - .azure-creds
    - .ghe-creds
  script:
    - poetry add azure-cli
    - poetry run ./azure_scripts/restore_azure_vm.sh
  rules:
    - if: "$CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH"
      changes:
        - azure_scripts/*.ps1
        - azure_scripts/*.sh
      when: manual
      allow_failure: true
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != 'schedule'"
      when: manual
      allow_failure: true
    - when: never
