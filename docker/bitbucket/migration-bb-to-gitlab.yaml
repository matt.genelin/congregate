version: "3"

volumes:
    glone-etc:
    glone-logs:
    glone-data:
    congregate_vol:

services:
    bitbucket:
        image: bitbucket-seed:latest
        restart: always
        ports:
            - "7990:7990"
            - "7999:7999"
        depends_on:
            - postgres
        hostname: "bitbucket"
        environment:
            - SETUP_LICENSE=$BITBUCKET_LICENSE
            - SETUP_BASEURL=http://bitbucket
            - SETUP_SYSADMIN_USERNAME=admin
            - SETUP_SYSADMIN_PASSWORD=$BITBUCKET_PASSWORD
            - SETUP_SYSADMIN_DISPLAYNAME=John Doe
            - SETUP_SYSADMIN_EMAILADDRESS=sysadmin@yourcompany.com
            - ELASTICSEARCH_ENABLED=false
            - JDBC_DRIVER=org.postgresql.Driver
            - JDBC_USER=db_user
            - JDBC_PASSWORD=db_password
            - JDBC_URL=jdbc:postgresql://postgres:5432/bitbucket
        volumes:
            - $CONGREGATE_PATH/test-data/bitbucket:/var/atlassian/application-data/bitbucket
        networks:
            bitbucket:

    gitlab-web:
        image: gitlab/gitlab-ee:latest
        container_name: gitlab-web
        hostname: gitlab-web-local.com
        volumes:
            - "glone-etc:/etc/gitlab"
            - "glone-logs:/var/log/gitlab"
            - "glone-data:/var/opt/gitlab"
        ports:
            - "22:22"
            - "8080:80"
            - "8443:443"
            - "9090:9090"
        networks:
            # Gitlab will build a direct stream for bitbucket repo to gitlab project imports so both networks are required
            congregate:
            bitbucket:

    congregate:
        image: registry.gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate:rolling-debian
        container_name: congregate
        hostname: congregate
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock
            - congregate_vol:/opt/congregate/data
        ports:
            - 8000:8000
        entrypoint: "/bin/bash -c 'sleep infinity'"
        networks:
            # Congregate will handle data loads import/export between bitbucket and gitlab from most migration activities so both networks are required
            congregate:
            bitbucket:

    postgres:
        image: postgres:11.8-alpine
        restart: always
        ports:
            - "5432:5432"
        env_file:
            - database.env
        volumes:
            - $CONGREGATE_PATH/test-data/database:/var/lib/postgresql/data/
        hostname: "postgres"
        networks:
            bitbucket:

networks:
    # Create seperate subnets for gitlab and bitbucket so gitlab will be able to send outbound requests to bitbucket on a "non local" network
    # By default, gitlab prevents outbound requests on a local network
    congregate:
        ipam:
            driver: default
            config:
                - subnet: 173.28.0.0/29
    bitbucket:
        ipam:
            driver: default
            config:
                - subnet: 173.29.0.0/29
